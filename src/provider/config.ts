const PROTOCOL = 'https';
const BASEURL = `${PROTOCOL}://jsonplaceholder.typicode.com`;
export const GetMessages = `${BASEURL}/posts`;
export const GetMessage = (id) => `${BASEURL}/posts/${id}`;
