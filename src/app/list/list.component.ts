import { Component, OnInit , OnDestroy} from '@angular/core';
import { Message } from 'src/model/message.model';
import { ServerService } from 'src/service/server.service';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  messages: Message[] = new Array<Message>();
  private messageSubscription: Subscription;
  constructor(
    private serverService: ServerService,
    private router: Router
  ) { }

  ngOnInit() {
    this.getMessages();
  }

  getMessages() {
    this.messageSubscription = this.serverService.getAllMessages().subscribe(messages => {
      console.log(messages);
      this.messages = messages as Message[];
    });
  }
  showDetail(id) {
    console.log(id)
    this.router.navigateByUrl(`messages/${id}`);
  }

  // for clean memory
  ngOnDestroy() {
    this.messageSubscription.unsubscribe();
  }
}
