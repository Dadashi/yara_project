import { Component, OnInit } from '@angular/core';
import {  Router } from '@angular/router';
import { ServerService } from 'src/service/server.service';
import { Subscription } from 'rxjs/internal/Subscription';
import { Message } from 'src/model/message.model';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {

  private messageSubscription: Subscription;
  message: Message = new Message();
  constructor(
    private route: Router,
    private serverService: ServerService

  ) { }

  ngOnInit() {
    const urlParts = this.route.url.split('/');
    const messageId = urlParts[urlParts.length - 1];
    this.getMessage(messageId);
    console.log(messageId);
  }
  getMessage(messageId) {
    this.serverService.getMessagesById(messageId).subscribe((message) => {
      console.log(message)
    this.message = message as Message;
    });
  }
}
