import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { GetMessages, GetMessage } from 'src/provider/config';

@Injectable({
  providedIn: 'root'
})
export class ServerService {

  constructor(
    private http: HttpClient
  ) { }

  getAllMessages() {
    return this.http.get(GetMessages);
  }
  getMessagesById(messageId) {
    return this.http.get(GetMessage(messageId));
  }

}
